package kz.aitu.Retake.cookie;


import kz.aitu.Retake.entity.User;

public class Cookie {
    private User user;
    private static Cookie instance;

    private Cookie(User user) {
        this.user = user;
    }

    public static void setUser(User user) {
        instance = new Cookie(user);
    }

    public static User getUser() {
        if (instance == null) {
            return null;
        }
        return instance.user;
    }
}
