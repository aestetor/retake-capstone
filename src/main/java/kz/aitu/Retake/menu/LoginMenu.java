package kz.aitu.Retake.menu;

import kz.aitu.Retake.cookie.Cookie;
import kz.aitu.Retake.entity.User;
import kz.aitu.Retake.repo.UserRepo;

import java.util.Scanner;

public class LoginMenu implements IMenu {
    private User user;

    public LoginMenu(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public void menu() throws Exception {
        Scanner scan = new Scanner(System.in);
        String username, password;
        System.out.println("Enter username:");
        username = scan.nextLine();
        System.out.println("Enter password:");
        password = scan.nextLine();
        user = new User(0, username, password);
        Cookie.setUser(user);
    }
}
