package kz.aitu.Retake.controller;

import kz.aitu.Retake.repo.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
@AllArgsConstructor



public class UserController {
    private UserRepository repository;
    @GetMapping("/api/v1/users")
    public ResponseEntity<?> getList() {
        return ResponseEntity.ok(repository.findAll());
    }
}
