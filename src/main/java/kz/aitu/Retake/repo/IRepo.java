package kz.aitu.Retake.repo;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

public interface IRepo {
    boolean validate() throws Exception;
    void insert() throws Exception;
    void update() throws SQLException;
    void delete() throws SQLException;
}
