package kz.aitu.Retake.repo;

import kz.aitu.Retake.connection.DatabaseConnection;
import kz.aitu.Retake.entity.User;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserRepo implements IRepo {
    private User user;

    public UserRepo(User user) {
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean validate() throws Exception {
        String username = user.getUsername();
        String password = user.getPassword();
        ResultSet rs = DatabaseConnection.getInstance().getConnection().createStatement().executeQuery("SELECT * FROM user WHERE username = '"+username+"' AND password = PASSWORD('"+password+"')");
        if(rs.next()) {
            return true;
        }
        return false;
    }

    @Override
    public void insert() throws Exception {
        String username = user.getUsername();
        String password = user.getPassword();
        DatabaseConnection.getInstance().getConnection().createStatement().execute("INSERT INTO user (username, password) VALUES ('" + username + "', PASSWORD('" + password + "'))");
    }

    @Override
    public void update() throws SQLException {
        int id = user.getId();
        String username = user.getUsername();
        String password = user.getPassword();
        DatabaseConnection.getInstance().getConnection().createStatement().execute("UPDATE user SET username = '" + username + "' AND password = '" + password + "' WHERE id = '" + id + "'");
    }

    @Override
    public void delete() throws SQLException {
        int id = user.getId();
        DatabaseConnection.getInstance().getConnection().createStatement().execute("DELETE FROM user WHERE id = '" + id + "'");
    }
}
